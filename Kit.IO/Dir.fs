﻿namespace Kit.IO

open System.IO

type Directory =

  static member copy (sourceDirectory, targetDirectory, ?fileAccept, ?dirAccept, ?tryDeleteOld) =
    let rec copyAll (source: DirectoryInfo, target: DirectoryInfo) =
      if Directory.Exists target.FullName = false then Directory.CreateDirectory target.FullName |> ignore
      for fi in source.GetFiles() do
        let fn () =
          fi.CopyTo (Path.Combine(target.ToString(), fi.Name), true) |> ignore
          match tryDeleteOld with Some true -> (try fi.Delete() with _ -> ()) | _ -> ()
        match fileAccept with Some fileAccept when fileAccept fi -> fn () | None -> fn () | _ -> ()
      for diSourceSubDir in source.GetDirectories() do
        let fn () =
          let nextTargetSubDir = target.CreateSubdirectory diSourceSubDir.Name
          copyAll (diSourceSubDir, nextTargetSubDir)
        match dirAccept with Some dirAccept when dirAccept diSourceSubDir -> fn () | None -> fn () | _ -> ()
        
    copyAll(DirectoryInfo sourceDirectory, DirectoryInfo targetDirectory)