﻿namespace Utilities.IO

open Kit
open System.Collections.Generic
open System.Text.RegularExpressions

type ZamlType =
  | ZamlS of string
  | ZamlI of int
  | ZamlF of float
  | ZamlB of bool
  | ZamlT of float triple
  | ZamlSeq of (string * ZamlType) seq

  member o.toList =
    match o with
      | ZamlSeq zs -> zs :?> List<string * ZamlType>
      | _ -> failwith "Does not exist."

  member o.tryFind (name: string) =
    match o with
      | ZamlSeq zs -> zs |> Seq.tryPick (fun (namei, zi) -> if name = namei then Some zi else zi.tryFind name)
      | _ -> None

  member o.tryFind (path: string seq) =
    path |> Seq.fold (fun zsi name ->
      match zsi with
        | Some (ZamlSeq s) ->
          s |> Seq.tryFind (fun (namei, _ ) -> namei = name) |> Option.map snd
        | _ -> None ) (Some o)

  member o.find (name: string) = match o.tryFind name with Some v -> v | None -> failwithf "Not found: %s" name
  member o.find (path: string seq) = match o.tryFind path with Some v -> v | None -> failwithf "Not found: %s" (path |> String.concat ", ") 

type Zaml =

  static member private ofString str =
    let res = System.Text.RegularExpressions.Regex.Match (str, "\\[(.+); (.+); (.+)\\]")
    if res.Groups.Count <> 4 then failwith "Triple: Cannot parse"
    triple (float res.Groups.[1].Value, float res.Groups.[2].Value, float res.Groups.[3].Value)

  static member toString (z: ZamlType) = match z with ZamlS s -> s | _ -> failwith "Zaml.toTriple: It is not a string"
  static member toTriple (z: ZamlType) = Zaml.ofString (Zaml.toString z)
  
  static member parse (s: string, ?separators) = 

    let seqStr, valStr, tab = defaultArg separators ("::", ":=", "  ")

    let countStartingTabs (s: string) =
      let rec fn (str: string) count =
        if str.StartsWith tab then
          let newStr = str.Substring(tab.Length)
          fn newStr (count + 1)
        else count
      fn s 0

    let rec set level nameValue (z: ZamlType) = 
      match z with
        | ZamlSeq zs when level > 0 ->
          let zs = (zs :?> List<string * ZamlType>)
          set (level - 1) nameValue (snd zs.[zs.Count - 1])
        | ZamlSeq zs ->
          let zs = (zs :?> List<string * ZamlType>)
          zs.Add nameValue
        | _ -> failwith "Error in ZaML set - parse"

    let rec appendString level (value: string) (z: ZamlType) = 
      match z with
        | ZamlSeq zs when level > 0 ->
          let zs = zs :?> List<string * ZamlType>
          appendString (level - 1) value (snd zs.[zs.Count - 1])
        | ZamlSeq zs -> 
          let zs = zs :?> List<string * ZamlType>
          match zs.[zs.Count - 1] with
            | name, ZamlS s -> zs.[zs.Count - 1] <- (name, ZamlS (s + value))
            | _ -> failwith "Error in Zaml - appendString"
        | _ -> failwith "Error in ZaML appendString - parse"

    let whole = ZamlSeq (List())

    s.Split([| '\n' |]) |> Array.fold (fun level si ->
        if si <> "" then

          let nameValue sep = 
            let siA =
              let regex = Regex ("\s*" + sep + "\s*")
              regex.Split(si, 2)
            siA.[0].Trim(), siA.[1].Trim()

          if si.Contains seqStr then
            let name, _ = nameValue seqStr
            let level = countStartingTabs si
            set level (name, ZamlSeq (List())) whole
            level
          elif si.Contains valStr then
            let name, value = nameValue valStr
            let level = countStartingTabs si
            set level (name, ZamlS value) whole
            level
          else
            appendString level si whole
            level
        else level ) 0
      |> ignore

    whole

  static member stringify (input: ZamlType, ?separators) =
    let seqStr, valStr, tab = defaultArg separators ("::", ":=", "  ")
    let rec stringify level (name, value: ZamlType) =
      let newLine =
        if name <> "--Root--" then
          let sep = match value with ZamlSeq _ -> seqStr | _ -> valStr
          let v =
            let inline s v = string v
            match value with ZamlSeq _ -> "" | ZamlS a -> s a | ZamlI a -> s a | ZamlF a -> s a | ZamlB a -> s a | ZamlT a -> s a
          let makeTabs level tab =
            let rec append str i = if i > 0 then append (str + tab) (i - 1) else str
            append "" level
          sprintf "%s%s %s %s\n" (makeTabs level tab) name sep v
        else ""
      match value with
        | ZamlSeq s -> s |> Seq.fold (fun cum nameValue -> cum + stringify (level + 1) nameValue) newLine
        | _ -> newLine
    ("--Root--", input) |> stringify -1

  static member itemify (input: ZamlType) =
    let ll = List()
    let line = ref 0
    let rec itemify level (name, value: ZamlType) =
      let _newLine =
        if name <> "--Root--" then
          let v =
            let inline s v = string v
            match value with ZamlSeq _ -> "" | ZamlS a -> s a | ZamlI a -> s a | ZamlF a -> s a | ZamlB a -> s a | ZamlT a -> s a
          ll.Add (!line, level, name, v)
          incr line
        else ()
      match value with
        | ZamlSeq s -> 
          //incr line
          s |> Seq.iter (fun nameValue -> itemify (level + 1) nameValue)
        | _ -> ()
    ("--Root--", input) |> itemify -1
    ll.ToArray()
