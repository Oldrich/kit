﻿namespace Kit.Tests.Math.Geometry

open NUnit.Framework
open Kit
open Kit.Math.Geometry

[<TestFixture>]
type IsPoint_Tests() =

  [<Test>]
  member o.inPolygon1() =
    let point = triple(2.8, 3.6, -12.5)
    let polygon = [
      triple(0.,0.,0.)
      triple(2.,0.,0.)
      triple(2.,3.,0.)
      triple(0.,3.,0.) ]
    let isInPoly = IsPoint.inPolygon(point, polygon)
    Assert.That(isInPoly, Is.False)

  [<Test>]
  member o.inPolygon2() =
    let point = triple(0., 0., 0.)
    let polygon = [
      triple(0.,0.,0.)
      triple(2.,0.,0.)
      triple(2.,3.,0.)
      triple(0.,3.,0.) ]
    let isInPoly = IsPoint.inPolygon(point, polygon)
    Assert.That(isInPoly, Is.True)

  [<Test>]
  member o.inPolygon3() =
    let point = triple(1., 1., 0.)
    let polygon = [
      triple(0.,0.,0.)
      triple(2.,0.,0.)
      triple(2.,3.,0.)
      triple(0.,3.,0.) ]
    let isInPoly = IsPoint.inPolygon(point, polygon)
    Assert.That(isInPoly, Is.True)

  [<Test>]
  member o.inPolygon4() =
    let point = triple(1., 1., 0.)
    let polygon = [
      triple(0.,0.,0.)
      triple(0.,3.,0.)
      triple(2.,3.,0.)
      triple(2.,0.,0.)
    ]
    let isInPoly = IsPoint.inPolygon(point, polygon)
    Assert.That(isInPoly, Is.True)

  [<Test>]
  member o.inPolygon5() =
    let point = triple(1., -1., 0.)
    let polygon = [
      triple(0.,0.,0.)
      triple(0.,3.,0.)
      triple(2.,3.,0.)
      triple(2.,0.,0.)
    ]
    let isInPoly = IsPoint.inPolygon(point, polygon)
    Assert.That(isInPoly, Is.False)

  [<Test>]
  member o.inPolygon6() =
    let point = triple(1., 0., 0.)
    let polygon = [
      triple(0.,0.,0.)
      triple(0.,3.,0.)
      triple(2.,3.,0.)
      triple(2.,0.,0.)
    ]
    let isInPoly = IsPoint.inPolygon(point, polygon)
    Assert.That(isInPoly, Is.True)

  [<Test>]
  member o.inPolygon7() =
    let point = triple(1., 0., 0.)
    let polygon = [
      triple(0.,0.,0.)
      triple(2.,3.,0.)
      triple(0.,3.,0.)
      triple(2.,0.,0.)
    ]
    let isInPoly = IsPoint.inPolygon(point, polygon)
    Assert.That(isInPoly, Is.False)