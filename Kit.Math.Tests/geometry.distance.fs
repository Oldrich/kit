﻿namespace Kit.Tests.Math.Geometry

open NUnit.Framework
open Kit
open Kit.Math.Geometry

[<TestFixture>]
type Distance_Tests() =

  [<Test>]
  member o.segmentPoint1() =
    let segment = triple(0.,1.,1.), triple(0.,1.,2.)
    let point = triple(0.5,0.,0.)
    let spoint = Distance.segmentPoint(segment, point)
    Assert.That(spoint, Is.EqualTo(triple(0.,1.,1.)))

  [<Test>]
  member o.segmentPoint2() =
    let segment = triple(0.,1.,1.), triple(0.,1.,2.)
    let point = triple(0.,0.,2.1)
    let spoint = Distance.segmentPoint(segment, point)
    Assert.That(spoint, Is.EqualTo(triple(0.,1.,2.)))

  [<Test>]
  member o.segmentPoint3() =
    let segment = triple(0.,1.,1.), triple(0.,1.,2.)
    let point = triple(0.5,0.8,1.5)
    let spoint = Distance.segmentPoint(segment, point)
    Assert.That(spoint, Is.EqualTo(triple(0.,1.,1.5)))

  [<Test>]
  member o.segmentSegment1() =
    let segment1 = triple(0.,1.,1.), triple(0.,1.,2.)
    let segment2 = triple(-2.,1.,1.), triple(-1.,1.,1.)
    let points = Distance.segmentSegment(segment1, segment2)
    let shouldBe = triple(0.,1.,1.), triple(-1.,1.,1.)
    Assert.That(points, Is.EqualTo shouldBe)

  [<Test>]
  member o.segmentSegment2() =
    let segment1 = triple(0.,1.,1.), triple(0.,1.,2.)
    let segment2 = triple(-2.,2.,1.5), triple(2.,2.,1.5)
    let points = Distance.segmentSegment(segment1, segment2)
    let shouldBe = triple(0.,1.,1.5), triple(0.,2.,1.5)
    Assert.That(points, Is.EqualTo shouldBe)

  [<Test>]
  member o.segmentSegment3() =
    let segment1 = triple(0.,1.,1.), triple(0.,1.,2.)
    let segment2 = triple(-2.,1.,1.5), triple(2.,1.,1.5)
    let points = Distance.segmentSegment(segment1, segment2)
    let shouldBe = triple(0.,1.,1.5), triple(0.,1.,1.5)
    Assert.That(points, Is.EqualTo shouldBe)