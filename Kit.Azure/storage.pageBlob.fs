﻿module Kit.Azure.Storage.PageBlob

open Microsoft.WindowsAzure
open Microsoft.WindowsAzure.StorageClient
open System.IO

let private getLastPageStartOffset (blob: CloudPageBlob) =
  let ranges = blob.GetPageRanges() |> Seq.toArray
  match ranges.Length with
  | 0 -> 0L
  | 1 -> ranges.[0].EndOffset + 1L - 512L
  | _ -> failwith "not supported"

let private readLastPage (blob: CloudPageBlob, blobOffset) =
  let stream = blob.OpenRead()
  let buffer = Array.zeroCreate 512
  stream.Seek(blobOffset, SeekOrigin.Begin) |> ignore
  stream.Read(buffer, 0, 512) |> ignore
  buffer

let private findEndInPage (bytes: byte[]) =
  let rec fn i =
    if i > -1 && bytes.[i] = 0uy then fn (i-1) else (i + 1)
  fn (bytes.Length - 1)

let getClient (accountName, key: string) =
  let creds = new StorageCredentialsAccountAndKey(accountName, key)
  let baseUri = sprintf "http://%s.blob.core.windows.net" accountName
  new CloudBlobClient(baseUri, creds)

let createBlobIfNotExist (blob: CloudPageBlob, blobSizeMB) =
  try blob.FetchAttributes() |> ignore
  with :? StorageClientException -> blob.Create (int64 blobSizeMB * 1024L * 1024L)

let append fn blob =
  let blobOffset = getLastPageStartOffset blob
  let lastPage = readLastPage (blob, blobOffset)
  let endi = findEndInPage lastPage
  use m = new MemoryStream()
  m.Write (lastPage, 0, endi)
  m.Seek(int64 endi, SeekOrigin.Begin) |> ignore
  use w = new BinaryWriter(m)
  fn w
  w.Flush()
  let capacity = (int m.Length / 512 + 1) * 512
  for i in int m.Length .. capacity - 1 do m.WriteByte 0uy
  m.Seek(0L, SeekOrigin.Begin) |> ignore
  blob.WritePages(m, blobOffset)
