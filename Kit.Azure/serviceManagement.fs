﻿module Kit.Azure.ServiceManagement.Change

open System.IO
open System.Xml.Linq
open Kit

module Internal =

  let get name (xml: XContainer) =
    xml.Descendants() |> Seq.find (fun d -> d.Name.LocalName = name)

  let postChanges (uri, xml, certificatePath, certificatePassword) =
    let uri' = uri + "/?comp=config"
    ServiceManagement.Request.post (uri', xml, certificatePath, certificatePassword)

  let createXmlOfChanges (config: XElement) =
    let config64' = String.toBase64 (config.ToString())
    let wa = XNamespace.Get "http://schemas.microsoft.com/windowsazure"
    let xConfig = XElement(wa + "Configuration", config64')
    let xChangeConfig = XElement(wa + "ChangeConfiguration", xConfig)
    let xml = new XDocument()
    xml.Add xChangeConfig
    xml.Declaration <- XDeclaration("1.0", "UTF-8", "no")
    xml

  let changeInstanceCount (config: XElement, roleName, func) =
    let instance =
      config.Descendants()
      |> Seq.find (fun d ->
        d.Name.LocalName = "Role" &&
        d.Attribute(XName.Get "name").Value = roleName )
      |> get "Instances"
    let count = int (instance.Attribute(XName.Get "count").Value)
    instance.SetAttributeValue(XName.Get "count", func count)

  let requestConfig (uri, certificatePath, certificatePassword) =
    let config64 =
      let xml = ServiceManagement.Request.get (uri, certificatePath, certificatePassword)
      get "Configuration" xml
    XElement.Parse (String.ofBase64 config64.Value)

  let composeUri (subscriptionId, serviceName, isProduction) =
    let deploymentSlot = if isProduction then "Production" else "Staging"
    sprintf "https://management.core.windows.net/%s/services/hostedservices/%s/deploymentslots/%s"
      subscriptionId serviceName deploymentSlot

let instanceCount (getCount, subscriptionId, serviceName, isProduction, roleName, certificatePath, certificatePassword) =
  let uri = Internal.composeUri (subscriptionId, serviceName, isProduction)
  let config = Internal.requestConfig (uri, certificatePath, certificatePassword)
  Internal.changeInstanceCount (config, roleName, getCount)
  let xml = Internal.createXmlOfChanges config
  Internal.postChanges (uri, xml, certificatePath, certificatePassword)