﻿module ServiceManagement.Request

open System.IO
open System.Net
open System.Xml.Linq
open System.Security.Cryptography.X509Certificates

let getCertificate(path, password: string) =
  use stream =
    let assembly = System.Reflection.Assembly.GetEntryAssembly()
    assembly.GetManifestResourceStream path
  use ms = new MemoryStream()
  stream.CopyTo ms
  new X509Certificate2(ms.ToArray(), password);

let private createHttpWebRequest (uri: string, httpWebRequestMethod: string, certPath: string, certPass) =
  let httpWebRequest = HttpWebRequest.CreateHttp uri
  httpWebRequest.Method <- httpWebRequestMethod
  httpWebRequest.Headers.Add("x-ms-version", "2011-02-25")
  httpWebRequest.ContentType <- "application/xml"
  let cert = getCertificate(certPath, certPass);
  let res = httpWebRequest.ClientCertificates.Add cert
  httpWebRequest

let get (uri: string, certificatePath, certificatePassword) =
  let httpWebRequest = createHttpWebRequest(uri, "GET", certificatePath, certificatePassword)
  use response = httpWebRequest.GetResponse() :?> HttpWebResponse
  use responseStream = response.GetResponseStream()
  XDocument.Load responseStream

let post (uri: string, xml: XDocument, certificatePath, certificatePassword) =
  let httpWebRequest = createHttpWebRequest(uri, "POST", certificatePath, certificatePassword)
  use requestStream = httpWebRequest.GetRequestStream()
  use streamWriter = new StreamWriter(requestStream, System.Text.UTF8Encoding.UTF8)
  xml.Save(streamWriter, SaveOptions.DisableFormatting)
  use response = httpWebRequest.GetResponse() :?> HttpWebResponse
  let requestId = response.Headers.["x-ms-request-id"]
  requestId
