﻿namespace Kit.IO.Tests

open Kit
open Kit.IO.XML
open System.Xml.Linq
open NUnit.Framework

[<TestFixture>]
type XML_Tests() =

  [<Test>]
  member o.String_xn() =
    Assert.That("test".xn, Is.EqualTo(XName.Get "test"))

  [<Test>]
  member o.String_xa() =
    let input = ("test".xa 4.0).ToString()
    let output = XAttribute(XName.Get "test", 4.0).ToString()
    Assert.That(input, Is.EqualTo output)

  [<Test>]
  member o.String_xe() =
    let input = "test".xe(4.0).ToString()
    let output = XElement(XName.Get "test", 4.0).ToString()
    Assert.That(input, Is.EqualTo output)

  [<Test>]
  member o.String_xroot() =
    let input = "test".xroot(4.0).ToString()
    let output = XElement(XName.Get "test", 4.0).ToString()
    Assert.That(input, Is.EqualTo output)

  [<Test>]
  member o.String_xt() =
    let input = "test".xt(triple(4.0,1.0,2.0)).ToString()
    let arr = [
      XAttribute(XName.Get "x", 4.0)
      XAttribute(XName.Get "y", 1.0)
      XAttribute(XName.Get "z", 2.0)
    ]
    let output = XElement(XName.Get "test", arr).ToString()
    Assert.That(input, Is.EqualTo output)


    

