﻿namespace Kit.Tests.Math

open NUnit.Framework
open Kit

[<TestFixture>]
type Triple_Tests() =

  let input = triple(1.,2.,3.)

  [<Test>] member o.x() = Assert.That(input.x, Is.EqualTo 1.)

  [<Test>] member o.y() = Assert.That(input.y, Is.EqualTo 2.)

  [<Test>] member o.z() = Assert.That(input.z, Is.EqualTo 3.)
    
  [<Test>]
  member o.item([<Values(0,1,2)>] i: int) =
    Assert.That(input.[i], Is.EqualTo(1. + float i))

  [<Test>]
  member o.ToString() =
    Assert.That(input.ToString(), Is.EqualTo "[1; 2; 3]")

  [<Test>]
  member o.ToString2( [<Values("[2; 2; 3]","[1; 3; 3]","[1; 2; 4]")>] outcome: string) =
    Assert.That(input.ToString(), Is.Not.EqualTo outcome)

  [<Test>]
  member o.Equals() =
    Assert.That(input, Is.EqualTo(triple(1.,2.,3.)))

  [<Test>]
  member o.Equals2() =
    Assert.That(input, Is.Not.EqualTo(triple(2.,2.,3.)))

  [<Test>]
  member o.GetHashCode() =
    let h1 = triple(1.152,-2.235,3.889).GetHashCode()
    let h2 = triple(1.152,-2.235,3.889).GetHashCode()
    Assert.That(h1, Is.EqualTo h2)

  [<Test>]
  member o.GetHashCode2() =
    let numberOfValues = 100000
    let values =
      let get =
        let rnd = System.Random()
        fun () -> 1000.0 * rnd.NextDouble() - 500.0
      Array.init numberOfValues (fun _ -> triple(get(), get(),get()).GetHashCode() )
    let numberOfUniqueValues = values |> Seq.distinct |> Seq.length
    let fraction = float (numberOfValues - numberOfUniqueValues) / float numberOfValues
    Assert.That(fraction, Is.LessThan 0.01)

  [<Test>]
  member o.get_Zero() =
    let output = triple<int>.get_Zero<int>()
    Assert.That(output, Is.EqualTo(triple(0,0,0)))

  [<Test>]
  member o.op_Multiply1() =
    Assert.That(4.0 * input, Is.EqualTo(triple(4.,8.,12.)))

  [<Test>]
  member o.op_Multiply2() =
    Assert.That(input * 4.0, Is.EqualTo(triple(4.,8.,12.)))

  [<Test>]
  member o.op_Multiply3() =
    let mat =
      array2D [
        [ 1.; 2.; 3. ]
        [ 4.; 5.; 6. ]
        [ 7.; 8.; 9. ]
      ]
    Assert.That(mat * input, Is.EqualTo(triple(14.,32.,50.)))

  [<Test>]
  member o.op_MultiplyByEl1() =
    Assert.That(input .* input, Is.EqualTo(triple(1.,4.,9.)))

  [<Test>]
  member o.op_Division1() =
    Assert.That(input / 0.1, Is.EqualTo(triple(10.,20.,30.)))

  [<Test>]
  member o.op_Addition1() =
    Assert.That(2.0 + input, Is.EqualTo(triple(3.,4.,5.)))

  [<Test>]
  member o.op_Addition2() =
    Assert.That(input + 2.0, Is.EqualTo(triple(3.,4.,5.)))

  [<Test>]
  member o.op_Addition3() =
    Assert.That(input + input, Is.EqualTo(triple(2.,4.,6.)))

  [<Test>]
  member o.op_Addition4() =
    Assert.That(input + (1.,2.,3.), Is.EqualTo(triple(2.,4.,6.)))

  [<Test>]
  member o.op_Addition5() =
    Assert.That((1.,2.,3.) + input, Is.EqualTo(triple(2.,4.,6.)))

  [<Test>]
  member o.op_Subtraction1() =
    Assert.That(3.0 - input, Is.EqualTo(triple(2.,1.,0.)))

  [<Test>]
  member o.op_Subtraction2() =
    Assert.That(input - 3.0, Is.EqualTo(triple(-2.,-1.,0.)))

  [<Test>]
  member o.op_Subtraction3() =
    Assert.That(input - input, Is.EqualTo(triple(0.,0.,0.)))

  [<Test>]
  member o.op_Subtraction4() =
    Assert.That(input - (1.,2.,3.), Is.EqualTo(triple(0.,0.,0.)))

  [<Test>]
  member o.op_Subtraction5() =
    Assert.That((1.,2.,3.) - input, Is.EqualTo(triple(0.,0.,0.)))

  [<Test>]
  member o.unary_Subtraction() =
    Assert.That(-input, Is.EqualTo(triple(-1.,-2.,-3.)))

  [<Test>]
  member o.op_PowByEl() =
    let output = input.**2.0
    Assert.That(output, Is.EqualTo(triple(1.,4.,9.)))
