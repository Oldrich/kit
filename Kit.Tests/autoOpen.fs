﻿namespace Kit.Tests

open NUnit.Framework
open Kit

[<TestFixture>]
type AutoOpen_Tests() =

  [<Test>]
  member o.defaultFn() =
    let output = defaultFn (Some 5.0) (fun () -> failwith "")
    Assert.That(output, Is.EqualTo 5.0)

  [<Test>]
  member o.defaultFn2() =
    let output = defaultFn None (fun () -> 5.0)
    Assert.That(output, Is.EqualTo 5.0)