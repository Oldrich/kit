﻿namespace Kit

type Tuple =
  static member inline fst ((a,_)) = a
  static member inline fst ((a,_,_)) = a
  static member inline fst ((a,_,_,_)) = a
  static member inline fst ((a,_,_,_,_)) = a
  static member inline fst ((a,_,_,_,_,_)) = a
  static member inline fst ((a,_,_,_,_,_,_)) = a
  static member inline fst ((a,_,_,_,_,_,_,_)) = a

  static member inline snd ((_,b)) = b
  static member inline snd ((_,b,_)) = b
  static member inline snd ((_,b,_,_)) = b
  static member inline snd ((_,b,_,_,_)) = b
  static member inline snd ((_,b,_,_,_,_)) = b
  static member inline snd ((_,b,_,_,_,_,_)) = b

  static member inline third ((_,_,c)) = c
  static member inline third ((_,_,c,_)) = c
  static member inline third ((_,_,c,_,_)) = c
  static member inline third ((_,_,c,_,_,_)) = c
  static member inline third ((_,_,c,_,_,_,_)) = c

  static member inline fourth ((_,_,_,d)) = d
  static member inline fourth ((_,_,_,d,_)) = d
  static member inline fourth ((_,_,_,d,_,_)) = d
  static member inline fourth ((_,_,_,d,_,_,_)) = d

  static member inline fifth ((_,_,_,_,e)) = e
  static member inline fifth ((_,_,_,_,e,_)) = e
  static member inline fifth ((_,_,_,_,e,_,_)) = e

  static member inline sixth ((_,_,_,_,_,f)) = f
  static member inline sixth ((_,_,_,_,_,_,f)) = f

  static member inline seventh ((_,_,_,_,_,_,g)) = g