﻿[<RequireQualifiedAccess>]
module Kit.Triple

exception NumberOfElementsException of string

let ones = triple(1,1,1)
let zero = triple(0,0,0)
let onesf = triple(1.,1.,1.)
let zerof = triple(0.,0.,0.)

let inline create input = triple(input, input, input)

let inline cross (input1: _ triple, input2: _ triple) =
  triple( input1.y * input2.z - input1.z * input2.y,
          input1.z * input2.x - input1.x * input2.z,
          input1.x * input2.y - input1.y * input2.x )

let inline dot (input1: _ triple, input2: _ triple) =
  input1.x * input2.x + input1.y * input2.y + input1.z * input2.z

let inline exists func (input: _ triple) =
  func input.x || func input.y || func input.z

let inline init func =
  triple(func 0, func 1, func 2)

let inline iteri func (input: _ triple) =
  func 0 input.x; func 1 input.y; func 2 input.z

let inline map func (input: _ triple) =
  triple(func input.x, func input.y, func input.z)

let inline mapi (func: int->_->_) (input: _ triple) =
  triple(func 0 input.x, func 1 input.y, func 2 input.z)

let inline norm (input: _ triple) =
  sqrt (input.x * input.x + input.y * input.y + input.z * input.z)

let inline norm2 (input: _ triple) =
  input.x * input.x + input.y * input.y + input.z * input.z

let inline normalize (t: _ triple) =
  let nm = norm t
  if nm = 0. then
    raise (System.DivideByZeroException "The zero vector cannot be normalized")
  else t / nm

let inline ofArray (xyz: _ []) =
  if xyz.Length <> 3 then
    let message = sprintf "Cannot convert array %A of length %d into triple" xyz xyz.Length
    raise (NumberOfElementsException message)
  triple (xyz.[0], xyz.[1], xyz.[2])

let inline ofList (xyz: _ list) =
  if xyz.Length <> 3 then
    let message = sprintf "Cannot convert list %A of length %d into triple" xyz xyz.Length
    raise (NumberOfElementsException message)
  triple (xyz.[0], xyz.[1], xyz.[2])

let inline ofTuple (x, y, z) = triple(x, y, z)

let inline outer (input1: _ triple, input2: _ triple) =
  let a = Array2D.zeroCreate 3 3
  a.[0,0] <- input1.x * input2.x
  a.[0,1] <- input1.x * input2.y
  a.[0,2] <- input1.x * input2.z
  a.[1,0] <- input1.y * input2.x
  a.[1,1] <- input1.y * input2.y
  a.[1,2] <- input1.y * input2.z
  a.[2,0] <- input1.z * input2.x
  a.[2,1] <- input1.z * input2.y
  a.[2,2] <- input1.z * input2.z
  a

let inline toArray (input: _ triple) = [| input.x; input.y; input.z |]

let inline toFloat (input: _ triple) =
  triple(float input.x, float input.y, float input.z)

let inline toInt (input: _ triple) =
  triple(int input.x, int input.y, int input.z)

let inline toList (input: _ triple) = [ input.x; input.y; input.z ]

let inline toTuple (input: _ triple) = input.x, input.y, input.z