﻿namespace Kit.Collections

open Kit

type array3<'t> =

  val array: 't [][][]
  new (array: 't [][][]) = { array = array }

  member o.lx = o.array.Length
  member o.ly i = o.array.[i].Length
  member o.lz (i, j) = o.array.[i].[j].Length

  member o.Item
    with get (i,j,k) = o.array.[i].[j].[k]
    and  set (i,j,k) value = o.array.[i].[j].[k] <- value

  member o.Item
    with get (t: int triple) = o.array.[t.x].[t.y].[t.z]
    and set (t: int triple) value = o.array.[t.x].[t.y].[t.z] <- value
  
  member o.GetSlice(x1o,x2o, y1o,y2o, z1o,z2o) =
    let x1, y1, z1 = defaultArg x1o 0, defaultArg y1o 0, defaultArg z1o 0
    let arr =
      let x2 = defaultArg x2o (o.lx - 1)
      Array.zeroCreate (x2 - x1 + 1)
    for i in 0 .. arr.Length - 1 do
      let x = i + x1
      arr.[i] <-
        let y2 = defaultArg y2o (o.ly(x) - 1)
        Array.zeroCreate (y2 - y1 + 1)
      for j in 0 .. arr.[i].Length - 1 do
        let y = j + y1
        arr.[i].[j] <-
          let z2 = defaultArg z2o (o.lz(x,y) - 1)
          Array.zeroCreate (z2 - z1 + 1)
        for k in 0 .. arr.[i].[j].Length - 1 do
          let z = k + z1
          arr.[i].[j].[k] <- o.[x,y,z]
    array3 arr

[<RequireQualifiedAccess>]
module Array3 =

  let inline create (input: _ array3) value =
    array3(Array.init input.lx (fun i ->
      Array.init (input.ly i) (fun j ->
        Array.create (input.lz(i, j)) value )))

  let inline exists func (input: _ array3) =
    let rec existsR f (n,m) = (n <= m) && (f n || existsR f (n+1,m))
    (0, input.lx - 1) |> existsR (fun i ->
      (0, input.ly i - 1) |> existsR (fun j ->
        (0, input.lz(i, j) - 1) |> existsR (fun k ->
          func input.[i,j,k] )))

  let inline existsi (func: int triple -> _ -> bool) (input: _ array3) =
    let rec existsR f (n,m) = (n <= m) && (f n || existsR f (n+1,m))
    (0, input.lx - 1) |> existsR (fun i ->
      (0, input.ly i - 1) |> existsR (fun j ->
        (0, input.lz(i, j) - 1) |> existsR (fun k ->
          func (triple(i,j,k)) input.[i,j,k] )))

  let existsIndex (xyz: int triple) (input: _ array3) =
    xyz.x >= 0 && xyz.x < input.lx &&
    xyz.y >= 0 && xyz.y < input.ly xyz.x &&
    xyz.z >= 0 && xyz.z < input.lz(xyz.x, xyz.y)

  let inline fold func initialValue (input: _ array3) =
    let mutable result = initialValue
    for i in 0 .. input.lx - 1 do
      for j in 0 .. input.ly i - 1 do
        for k in 0 .. input.lz(i, j) - 1 do
          result <- func result input.[i,j,k]
    result

  let inline foldi (func: int triple -> _ -> _ -> _) initialValue (input: _ array3) =
    let mutable result = initialValue
    for i in 0 .. input.lx - 1 do
      for j in 0 .. input.ly i - 1 do
        for k in 0 .. input.lz(i, j) - 1 do
          result <- func (triple(i,j,k)) result input.[i,j,k]
    result

  let inline forall func (input: _ array3) =
    let rec forallR f (n,m) = (n > m) || (f n && forallR f (n+1,m))
    (0, input.lx - 1) |> forallR (fun i ->
      (0, input.ly i - 1) |> forallR (fun j ->
        (0, input.lz(i, j) - 1) |> forallR (fun k ->
          func input.[i,j,k] )))

  let inline foralli (func: int triple -> _ -> bool) (input: _ array3) =
    let rec forallR f (n,m) = (n > m) || (f n && forallR f (n+1,m))
    (0, input.lx - 1) |> forallR (fun i ->
      (0, input.ly i - 1) |> forallR (fun j ->
        (0, input.lz(i, j) - 1) |> forallR (fun k ->
          func (triple(i,j,k)) input.[i,j,k] )))

  let inline init (lengthsZ: int[,]) func =
    array3(Array.init (lengthsZ.GetLength 0) (fun i ->
      Array.init (lengthsZ.GetLength 1) (fun j ->
        Array.init lengthsZ.[i,j] (fun k -> func(triple(i, j, k)) ))))

  let inline iter func (input: _ array3) =
    for i in 0 .. input.lx - 1 do
      for j in 0 .. input.ly i - 1 do
        for k in 0 .. input.lz(i, j) - 1 do
          func input.[i,j,k]

  let inline iteri (func: int triple -> _ -> unit) (input: _ array3) =
    for i in 0 .. input.lx - 1 do
      for j in 0 .. input.ly i - 1 do
        for k in 0 .. input.lz(i, j) - 1 do
          func (triple(i,j,k)) input.[i,j,k]

  let inline zeroCreate (input: _ array3) =
    array3(Array.init input.lx (fun i ->
      Array.init (input.ly i) (fun j ->
        Array.zeroCreate (input.lz(i, j)) )) )

  let inline map func (input: _ array3) =
    let output = zeroCreate input
    for i in 0 .. input.lx - 1 do
      for j in 0 .. input.ly i - 1 do
        for k in 0 .. input.lz(i, j) - 1 do
          output.array.[i].[j].[k] <- func input.[i,j,k]
    output

  let inline mapi (func: int triple -> _ -> _) (input: _ array3) =
    let output = zeroCreate input
    for i in 0 .. input.lx - 1 do
      for j in 0 .. input.ly i - 1 do
        for k in 0 .. input.lz(i, j) - 1 do
          output.array.[i].[j].[k] <- func (triple(i,j,k)) input.[i,j,k]
    output

  let inline copy (input: _ array3) = input |> map id

  let inline max (input: _ array3) =
    let mutable maxV = input.[0,0,0]
    for i in 0 .. input.lx - 1 do
      for j in 0 .. input.ly i - 1 do
        for k in 0 .. input.lz(i, j) - 1 do
          if input.[i,j,k] > maxV then
            maxV <- input.[i,j,k]
    maxV

  let inline maxBy func (input: _ array3) =
    let mutable maxV = None
    for i in 0 .. input.lx - 1 do
      for j in 0 .. input.ly i - 1 do
        for k in 0 .. input.lz(i, j) - 1 do
          match maxV with
          | None -> maxV <- Some (func input.[i,j,k])
          | Some v ->
            let res = func input.[i,j,k]
            if res > v then maxV <- Some res
    maxV.Value

  let inline maxByi func (input: _ array3) =
    let mutable maxV = None
    for i in 0 .. input.lx - 1 do
      for j in 0 .. input.ly i - 1 do
        for k in 0 .. input.lz(i, j) - 1 do
          match maxV with
          | None -> maxV <- Some (func (triple(i,j,k)) input.[i,j,k])
          | Some v ->
            let res = func (triple(i,j,k)) input.[i,j,k]
            if res > v then maxV <- Some res
    maxV.Value

  let inline removeOption defaultValue (input: _ array3) =
    input |> map (fun v -> defaultArg v defaultValue)

  let inline sumBy func (input: _ array3) =
    let mutable acc = LanguagePrimitives.GenericZero<_>
    for i in 0 .. input.lx - 1 do
      for j in 0 .. input.ly i - 1 do
        for k in 0 .. input.lz(i, j) - 1 do
          acc <- Checked.(+) acc (func input.[i,j,k])
    acc

  let inline sumByi func (input: _ array3) =
    let mutable acc = LanguagePrimitives.GenericZero<_>
    for i in 0 .. input.lx - 1 do
      for j in 0 .. input.ly i - 1 do
        for k in 0 .. input.lz(i, j) - 1 do
          acc <- Checked.(+) acc (func (triple(i,j,k)) input.[i,j,k])
    acc

  let inline tryGet (t: int triple) (input: _ array3) =
    if  t.x >= 0 && t.x < input.lx &&
        t.y >= 0 && t.y < input.ly t.x &&
        t.z >= 0 && t.z < input.lz(t.x, t.y) then Some input.[t] else None


  //
  //  let maxByi fn (input: _ array3) =
  //    let mutable accv = input.[0,0,0]
  //    let mutable acc = fn 0 0 0 accv
  //    for x = 0 to input.lx - 1 do
  //      for y = 0 to input.ly i - 1 do
  //        for z = 0 to input.lxsZ.[x,y] - 1 do
  //          if x <> 0 || y <> 0 || z <> 0 then
  //            let currv = input.[x,y,z]
  //            let curr = fn x y z currv
  //            if curr > acc then
  //              acc <- curr
  //              accv <- currv
  //    accv
  //
  //  let minBy fn (input: _ array3) =
  //    let mutable accv = input.[0,0,0]
  //    let mutable acc = fn accv
  //    for x = 0 to input.lx - 1 do
  //      for y = 0 to input.ly i - 1 do
  //        for z = 0 to input.lxsZ.[x,y] - 1 do
  //          if x <> 0 || y <> 0 || z <> 0 then
  //            let currv = input.[x,y,z]
  //            let curr = fn currv
  //            if curr < acc then
  //              acc <- curr
  //              accv <- currv
  //    accv
  //
  //  let minByi fn (input: _ array3) =
  //    let mutable accv = input.[0,0,0]
  //    let mutable acc = fn 0 0 0 accv
  //    for x = 0 to input.lx - 1 do
  //      for y = 0 to input.ly i - 1 do
  //        for z = 0 to input.lxsZ.[x,y] - 1 do
  //          if x <> 0 || y <> 0 || z <> 0 then
  //            let currv = input.[x,y,z]
  //            let curr = fn x y z currv
  //            if curr < acc then
  //              acc <- curr
  //              accv <- currv
  //    accv
  //
  //  // Removes Some/None from the GArray. None is replaced by the first parameter.
  //  let removeOption e (input: _ array3) =
  //    ga |> map (fun el ->
  //      match el with
  //        | Some x -> x
  //        | None -> e )
  //
  //  let sum (input: _ array3) =
  //    let mutable acc = LanguagePrimitives.GenericZero<_>
  //    for x = 0 to input.lx - 1 do
  //      for y = 0 to input.ly i - 1 do
  //        for z = 0 to input.lxsZ.[x,y] - 1 do
  //          acc <- Checked.(+) acc input.[x,y,z]
  //    acc
  //
  //  let sumBy fn (input: _ array3) =
  //    let mutable acc = LanguagePrimitives.GenericZero<_>
  //    for x = 0 to input.lx - 1 do
  //      for y = 0 to input.ly i - 1 do
  //        for z = 0 to input.lxsZ.[x,y] - 1 do
  //          acc <- Checked.(+) acc (fn input.[x,y,z])
  //    acc
  //
  //  let sumByi fn (input: _ array3) =
  //    let mutable acc = LanguagePrimitives.GenericZero<_>
  //    for x = 0 to input.lx - 1 do
  //      for y = 0 to input.ly i - 1 do
  //        for z = 0 to input.lxsZ.[x,y] - 1 do
  //          acc <- Checked.(+) acc (fn x y z input.[x,y,z])
  //    acc
  //
  //  let unzip (input: _ array3) =
  //    let a = array3 input.lxsZ
  //    let b = array3 input.lxsZ
  //    for x = 0 to a.Length - 1 do
  //      for y = 0 to a.[i].Length - 1 do
  //        for z = 0 to a.lengthsZ.[x,y] - 1 do
  //          a.[x,y,z] <- fst input.[x,y,z]
  //          b.[x,y,z] <- snd input.[x,y,z]
  //    a, b
  //
  //  let average (input: _ array3) = LanguagePrimitives.DivideByInt (sum ga) input.lx
  //  let averageBy fn (input: _ array3) = LanguagePrimitives.DivideByInt (sumBy fn ga) input.lx
  //  let averageByi fn (input: _ array3) = LanguagePrimitives.DivideByInt (sumByi fn ga) input.lx
  //    
