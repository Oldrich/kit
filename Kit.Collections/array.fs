﻿[<RequireQualifiedAccess>]
module Kit.Collections.Array

open System.Collections.Generic

let inline averageByi (func: int -> 'T -> ^U) (input: 'T []) =
  let mutable acc = LanguagePrimitives.GenericZero< (^U) >
  for i in 0 .. input.Length - 1 do
    acc <- Checked.(+) acc (func i input.[i])
  LanguagePrimitives.DivideByInt< (^U) > acc input.Length

let inline choosei func input =
  let results = List()
  input |> Array.iteri (fun i value -> 
    match func i value with
      | Some result -> results.Add result
      | _ -> ()
  )
  results.ToArray()

let inline collecti func input =
  input |> Array.mapi func |> Array.concat

let inline distinct (input: _ []) =
  let hashset = HashSet(input)
  let result = Array.zeroCreate hashset.Count
  hashset.CopyTo(result)
  result

let inline distinctBy func (source: _ []) =
  source |> Seq.distinctBy func |> Array.ofSeq

let inline existsi func (input: _ []) =
  let rec fn i =
    if func i input.[i] then true
    elif i < input.Length - 1 then fn(i + 1)
    else false
  fn 0

let inline filteri (func: int->_->bool) (input: _ []) =
  let result = new List<_>()
  for i in 0 .. input.Length - 1 do 
    if func i input.[i] then
      result.Add input.[i] 
  result.ToArray()

let inline foldi (func: int->_->_->_) initialValue (input: _ []) = 
  let mutable result = initialValue
  for i in 0 .. input.Length - 1 do
    result <- func i result input.[i]
  result

let inline linspace (minValue: float, maxValue: float, n: int) =
  let dV = ((maxValue - minValue) / float (n - 1))
  let out = Array.zeroCreate n
  out.[out.Length - 1] <- maxValue
  for i in 0 .. n - 2 do out.[i] <- minValue + float i * dV
  out

let inline maxByi func (input: _ []) =
  let mutable maxElement = input.[0]
  let mutable maxValue = func 0 input.[0]
  for i in 1 .. input.Length - 1 do
    let currentValue = func i input.[i]
    if currentValue > maxValue then
      maxValue <- currentValue
      maxElement <- input.[i]
  maxElement

let inline minByi func (input: _ []) =
  let mutable maxElement = input.[0]
  let mutable maxValue = func 0 input.[0]
  for i in 1 .. input.Length - 1 do
    let currentValue = func i input.[i]
    if currentValue < maxValue then
      maxValue <- currentValue
      maxElement <- input.[i]
  maxElement

let inline partitioni (func: int->_->bool) (input: _ []) =
  let s1, s2 = List(), List()
  for i in 0 .. input.Length - 1 do
    if func i input.[i] then s1.Add input.[i]
    else s2.Add input.[i]
  s1.ToArray(), s2.ToArray()

let inline removeOption (defaultValue: 'T) (input: 'T option []): 'T [] =
  input |> Array.map (fun el ->
    match el with
      | Some(x) -> x
      | None -> defaultValue )

let inline sum (input: _ []) =
  let mutable acc = LanguagePrimitives.GenericZero<_>
  for i in 0 .. input.Length - 1 do
    acc <- Checked.(+) acc input.[i]
  acc

let inline sumBy (func: 'T -> ^U) (input: 'T []): ^U =
  let mutable acc = LanguagePrimitives.GenericZero< (^U) >
  for i in 0 .. input.Length - 1 do
    acc <- Checked.(+) acc (func input.[i])
  acc

let inline sumByi (func: int -> 'T -> ^U) (input: 'T []): ^U =
  let mutable acc = LanguagePrimitives.GenericZero< (^U) >
  for i in 0 .. input.Length - 1 do
    acc <- Checked.(+) acc (func i input.[i])
  acc

let inline tail (arr: _ []) =
  Array.init (arr.Length - 1) (fun i -> arr.[i+1])