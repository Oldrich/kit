﻿namespace Kit.Collections

open Kit

open System.Collections.Generic

type array4<'t> =

  val origin: int triple
  val array: 't array3 array3
  val nodes: int triple
  val offset: int
  val a3: 't array3
  val dirtyIndeces: (int triple * int triple) List
  
  new (array, origin, nodes, offset) =
    { array = array; origin = origin; nodes = nodes; offset = offset; a3 = array.[1,1,1]; dirtyIndeces = List () }

  member o.Item

    with get (t: int triple) =
      let loct = t - o.origin
      let inline getIndex x len = if x < 0 then 0 elif x < len then 1 else 2
      let ix = getIndex loct.x o.nodes.x
      let iy = getIndex loct.y o.nodes.y
      let iz = getIndex loct.z o.nodes.z
      let tx = match ix with 1 -> loct.x | 0 -> loct.x + o.offset | 2 -> loct.x - o.nodes.x | _ -> failwith "cannot happen"
      let ty = match iy with 1 -> loct.y | 0 -> loct.y + o.offset | 2 -> loct.y - o.nodes.y | _ -> failwith "cannot happen"
      let tz = match iz with 1 -> loct.z | 0 -> loct.z + o.offset | 2 -> loct.z - o.nodes.z | _ -> failwith "cannot happen"
      o.array.[ix,iy,iz].[tx,ty,tz]

    and set (t: int triple) (v: 't) =
      let loct = t - o.origin
      let inline getIndex x len = if x < 0 then 0 elif x < len then 1 else 2
      let ix = getIndex loct.x o.nodes.x
      let iy = getIndex loct.y o.nodes.y
      let iz = getIndex loct.z o.nodes.z
      let tx = match ix with 1 -> loct.x | 0 -> loct.x + o.offset | 2 -> loct.x - o.nodes.x | _ -> failwith "cannot happen"
      let ty = match iy with 1 -> loct.y | 0 -> loct.y + o.offset | 2 -> loct.y - o.nodes.y | _ -> failwith "cannot happen"
      let tz = match iz with 1 -> loct.z | 0 -> loct.z + o.offset | 2 -> loct.z - o.nodes.z | _ -> failwith "cannot happen"
      o.array.[ix,iy,iz].[tx,ty,tz] <- v
      if ix <> 1 || iy <> 1 || iz <> 1 then o.dirtyIndeces.Add (triple(ix, iy, iz), triple(tx, ty, tz))

