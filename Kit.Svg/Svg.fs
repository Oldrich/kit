﻿namespace Kit.SVG

open Kit.IO.XML
open System.Xml
open System.Xml.Linq
open System.IO

module private n =
  let space = XNamespace.Get "http://www.w3.org/2000/svg"

type Style() =
  member val xml = XElement(n.space + "style", [ "type".xa "text/css" ])
  member private o.add (value: obj) = o.xml.Add value; o
  member o.append text = o.add text
  member o.fromFile filePath =
    use stream = System.Reflection.Assembly.GetCallingAssembly().GetManifestResourceStream filePath
    use reader = new StreamReader(stream)
    o.add (reader.ReadToEnd())
  member inline o.addTo v =
    let xml = (^a : (member xml: XElement with get) (v))
    xml.Add(o.xml)

type Transform() =
  let r2d v = v / System.Math.PI * 180.
  member val text = "" with get, set
  member private o.add value = o.text <- o.text + " " + value; o
  member o.translate(x, y) = o.add(sprintf "translate(%A, %A)" x y)
  member o.scale(x, y) = o.add(sprintf "scale(%A, %A)" x y)
  member o.rotateDeg(angle, x, y) =
    o.add(sprintf "rotate(%A,%A,%A)" angle x y)
  member o.rotateRad(angle, x, y) =
    o.add(sprintf "rotate(%A,%A,%A)" (r2d angle) x y)
  member o.skewX(angle) = o.add(sprintf "skewX(%A)" angle)
  member o.skewY(angle) = o.add(sprintf "skewY(%A)" angle)
  member o.matrix(a,b,c,d,e,f) =
    o.add(sprintf "matrix(%A,%A,%A,%A,%A,%A)" a b c d e f)

type PathD() as self =
  let lst = ResizeArray()
  let f1 name a =
    lst.Add (sprintf "%s %A" name a); self
  let f2 name a b =
    lst.Add (sprintf "%s %A %A" name a b); self
  let f4 name a b c d =
    lst.Add (sprintf "%s %A %A %A %A" name a b c d); self
  let f6 name a b c d e f =
    lst.Add (sprintf "%s %A %A %A %A %A %A" name a b c d e f); self
  let f7 name a b c d e f g =
    lst.Add (sprintf "%s %A %A %A %A %A %A %A" name a b c d e f g); self
  /// move to - relative
  member o.m (x,y) = f2 "m" x y
  /// move to - absolute
  member o.M (x,y) = f2 "M" x y
  /// close path
  member o.z = "z"
  /// line to - relative
  member o.l (x,y) = f2 "l" x y
  /// line to - absolute
  member o.L (x,y) = f2 "L" x y
  /// horizontal line to - relative
  member o.h (x) = f1 "h" x
  /// horizontal line to - absolute
  member o.H (x) = f1 "H" x
  /// vertical line to - relative
  member o.v (y) = f1 "v" y
  /// vertical line to - absolute
  member o.V (y) = f1 "V" y
  /// cubic Bezier curve to - relative
  member o.c (x1, y1, x2, y2, x, y) = f6 "c" x1 y1 x2 y2 x y
  /// cubic Bezier curve to - absolute
  member o.C (x1, y1, x2, y2, x, y) = f6 "C" x1 y1 x2 y2 x y
  /// smooth cubic Bezier curve to - relative
  member o.s (x2, y2, x, y) = f4 "s" x2 y2 x y
  /// smooth cubic Bezier curve to - absolute
  member o.S (x2, y2, x, y) = f4 "S" x2 y2 x y
  /// quadratic Bezier curve to - relative
  member o.q (x1, y1, x, y) = f4 "q" x1 y1 x y
  /// quadratic Bezier curve to - absolute
  member o.Q (x1, y1, x, y) = f4 "Q" x1 y1 x y
  /// smooth quadratic Bezier curve to - relative
  member o.t (x, y) = f2 "t" x y
  /// smooth quadratic Bezier curve to - absolute
  member o.T (x, y) = f2 "T" x y
  /// elliptical arc - relative
  member o.a (rx, ry, xAxisRotation, largeArcFlag, sweepFlag, x, y) =
    f7 "a" rx ry xAxisRotation largeArcFlag sweepFlag x y
  /// elliptical arc - absolute
  member o.A (rx, ry, xAxisRotation, largeArcFlag, sweepFlag, x, y) =
    f7 "A" rx ry xAxisRotation largeArcFlag sweepFlag x y
  override o.ToString() =
    lst |> String.concat ","


type Basic<'t when 't :> Basic<'t>>(name: string) =
  member val xml = XElement(XNamespace.Get "http://www.w3.org/2000/svg" + name)
  member o.add (value: obj) = o.xml.Add value; o :?> 't
  member o.style (v: string) = o.add("style".xa v)
  member o.Class (v: string) = o.add("class".xa v)
  member o.transform (v: Transform) = o.add("transform".xa v.text)
  member o.id (v: string) = o.add("class".xa v)
  member inline o.addTo v =
    let xml = (^a : (member xml: XElement with get) (v))
    xml.Add(o.xml)
   
and Svg(width: float, height: float) as o =
  inherit Basic<Svg>("svg")
  do
    o.add("version".xa 1.1) |> ignore
    o.add("width".xa width) |> ignore
    o.add("height".xa height) |> ignore
    let a: Rect = Rect.New(0.,0.,width,height)
    a.style("fill:#D3D3D3;").addTo o

and G() =
  inherit Basic<G>("g")

and Defs() =
  inherit Basic<Defs>("defs")

and Path() =
  inherit Basic<Path>("path")
  member o.d (v: PathD) = o.add("d".xa(v.ToString()))

and Rect() =
  inherit Basic<Rect>("rect")
  member o.x v = o.add("x".xa v)
  member o.y v = o.add("y".xa v)
  member o.width v = o.add("width".xa v)
  member o.height v = o.add("height".xa v)
  member o.rx v = o.add("rx".xa v)
  member o.ry v = o.add("ry".xa v)
  static member New(x,y,width,height) =
    let r = Rect()
    r.x(x).y(y).width(width).height(height)

and Circle() =
  inherit Basic<Circle>("circle")
  member o.cx v = o.add("cx".xa v)
  member o.cy v = o.add("cy".xa v)
  member o.r v = o.add("r".xa v)
  static member New(cx,cy,r) =
    let c = Circle()
    c.cx(cx).cy(cy).r(r)

and Ellipse() =
  inherit Basic<Ellipse>("ellipse")
  member o.cx v = o.add("cx".xa v)
  member o.cy v = o.add("cy".xa v)
  member o.rx v = o.add("rx".xa v)
  member o.ry v = o.add("ry".xa v)
  static member New(cx,cy,rx,ry) =
    let el = Ellipse()
    el.cx(cx).cy(cy).rx(rx).ry(ry)

and Line() =
  inherit Basic<Line>("line")
  member o.x1 v = o.add("x1".xa v)
  member o.x2 v = o.add("x2".xa v)
  member o.y1 v = o.add("y1".xa v)
  member o.y2 v = o.add("y2".xa v)
  static member New(x1,y1,x2,y2) =
    let el = Line()
    el.x1(x1).x2(x2).y1(y1).y2(y2)

and Polyline() =
  inherit Basic<Polyline>("polyline")
  member o.points (points: (_*_) seq) =
    let text = 
      points
      |> Seq.map (fun (x,y) -> sprintf "%A,%A" x y)
      |> String.concat " "
    o.add("points".xa text)

and Polygon() =
  inherit Basic<Polygon>("polygon")
  member o.points (points: (_*_) seq) =
    let text = 
      points
      |> Seq.map (fun (x,y) -> sprintf "%A,%A" x y)
      |> String.concat " "
    o.add("points".xa text)

and Text() =
  inherit Basic<Text>("text")
  member o.x v = o.add("x".xa v)
  member o.y v = o.add("y".xa v)
  member o.text v = o.add(v)
  member o.rotate  v = o.add("rotate".xa v)
  static member New(x, y, text, ?rotate) =
    let el = Text()
    match rotate with
    | Some r -> el.x(x).y(y).text(text).rotate(r)
    | None -> el.x(x).y(y).text(text)