﻿namespace Kit.Math

open Kit

[<Struct>]
[<CustomEquality>]
[<NoComparison>]
type quaternion =

  val w: float
  val t: float triple
  new (w, t) = { w = w; t = t}
  
  override o.ToString() =
    System.String.Format("[{0:G}; [{1:G}; {2:G}; {3:G}]]", o.w, o.t.x, o.t.y, o.t.z)

  override o.Equals(ob : obj) =
    match ob with
    | :? quaternion as v ->
      abs (o.w - v.w) < 1e-10 && o.t.Equals v.t
    | _ -> false

  override o.GetHashCode() = 
    let hash = 31 + o.w.GetHashCode()
    let hash = hash * 31 + o.t.x.GetHashCode()
    let hash = hash * 31 + o.t.y.GetHashCode()
    hash * 31 + o.t.z.GetHashCode()

  static member inline ( + ) (q1: quaternion, q2: quaternion) =
    quaternion(q1.w + q2.w, q1.t + q2.t)

  static member inline ( - ) (q1: quaternion, q2: quaternion) =
    quaternion(q1.w - q2.w, q1.t - q2.t)

  static member inline ( * ) (a, q: quaternion) = quaternion(a * q.w, a * q.t)

  static member inline ( / ) (q: quaternion, a) = quaternion(q.w / a, q.t / a)

  /// Hamilton product
  static member inline ( * ) (q1: quaternion, q2: quaternion) = 
    let w = q1.w * q2.w - Triple.dot(q1.t, q2.t)
    let t = q1.w * q2.t + q2.w * q1.t + Triple.cross(q1.t, q2.t)
    quaternion(w, t)