﻿[<RequireQualifiedAccess>]
module Kit.Math.Quaternion

open Kit

let zero = quaternion(1., Triple.zerof)

let inline conjugate (input: quaternion) = quaternion(input.w, -input.t)

let inline norm2 (input: quaternion) =
  input.w * input.w + Triple.dot(input.t, input.t)

let inline norm input = sqrt (norm2 input)

let inline inverse input = conjugate input / norm2 input

let inline normalize (q: quaternion) = q / norm q

