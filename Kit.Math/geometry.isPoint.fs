﻿[<RequireQualifiedAccess>]
module Kit.Math.Geometry.IsPoint

open Kit

let inPolygon (point: float triple, vertices: float triple seq) =
  let eps = 1e-10
  let verts = Array.ofSeq vertices
  let n = verts.Length
  let rec fn i sumAngle =
    let v1, v2 = verts.[i%n] - point, verts.[(i+1)%n] - point
    let m1, m2 = Triple.norm v1, Triple.norm v2
    if m1 < eps || m2 < eps then true
    elif i < n then
      let angle =
        let vdot = Triple.dot(v1, v2)
        acos (vdot / (m1 * m2))
      fn (i+1) (sumAngle + angle)
    else abs (sumAngle - 2. * System.Math.PI) < eps
  fn 0 0.