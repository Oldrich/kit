﻿namespace Kit.Math

open Kit

type versor = quaternion

[<RequireQualifiedAccess>]
module Versor =

  let inline ofYawPitchRoll (yaw, pitch, roll): versor =
    let q0 = cos(roll/2.)*cos(pitch/2.)*cos(yaw/2.)+sin(roll/2.)*sin(pitch/2.)*sin(yaw/2.)
    let q1 = sin(roll/2.)*cos(pitch/2.)*cos(yaw/2.)-cos(roll/2.)*sin(pitch/2.)*sin(yaw/2.)
    let q2 = cos(roll/2.)*sin(pitch/2.)*cos(yaw/2.)+sin(roll/2.)*cos(pitch/2.)*sin(yaw/2.)
    let q3 = cos(roll/2.)*cos(pitch/2.)*sin(yaw/2.)-sin(roll/2.)*sin(pitch/2.)*cos(yaw/2.)
    quaternion(q0, triple(q1, q2, q3))

  let inline ofAxisAngle (t: _ triple, angle): versor =
    if Triple.norm t < 10e-10 then Quaternion.zero
    else quaternion (cos(0.5*angle), (Triple.normalize t) * sin(0.5 * angle))

  let inline ofAngularVelocityVector (t: _ triple) =
    ofAxisAngle (t, Triple.norm t)

  let inline toAxisAngle (o: versor) =
    if o.w = 1. then triple(1.,0.,0.), 0.
    else o.t / sqrt(1. - o.w * o.w), 2. * acos o.w

  let inline toAngularVelocityVector (v: versor) =
    let t, angle = toAxisAngle v
    angle * Triple.normalize t

  let inline toMatrix (o: versor) =
    let w, x, y, z = o.w, o.t.x, o.t.y, o.t.z
    let w2, x2, y2, z2 = w*w, x*x, y*y, z*z
    let out = Array2D.zeroCreate 3 3
    out.[0,0] <- w2+x2-y2-z2
    out.[0,1] <- 2.*(x*y-w*z)
    out.[0,2] <- 2.*(x*z+w*y)
    out.[1,0] <- 2.*(x*y+w*z)
    out.[1,1] <- w2-x2+y2-z2
    out.[1,2] <- 2.*(y*z-w*x)
    out.[2,0] <- 2.*(x*z-w*y)
    out.[2,1] <- 2.*(y*z+w*x)
    out.[2,2] <- w2-x2-y2+z2
    out

  let inline rotateG2L (q: versor) (t: _ triple) =
    let vq = Quaternion.conjugate q * quaternion(0., t) * q
    vq.t

  let inline rotateL2G (q: versor) (t: _ triple) =
    let q = Quaternion.normalize q
    let vq = q * quaternion(0., t) * Quaternion.conjugate q
    vq.t