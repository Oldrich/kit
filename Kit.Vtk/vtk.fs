﻿namespace Kit.VTK

open Kitware.VTK
open System.Collections.Generic
open Kit
open Kit.Collections
open Kit.Math

module RedirectError =
  let toFile fileName =
    use fileOutputWindow = vtkFileOutputWindow.New()
    fileOutputWindow.SetFileName fileName
    fileOutputWindow.FlushOn()
    vtkOutputWindow.SetInstance fileOutputWindow

type PolyData (orn: float triple, areColors) =
  
  let append = vtkAppendPolyData.New ()
  let colors = vtkUnsignedCharArray.New ()
  let cylinders, spheres, ellipsoids, polygons, lines, points, planarPolygons = List (), List (), List (), List (), List (), List (), List ()

  do
    colors.SetNumberOfComponents 4
    colors.SetName "Colors"

  let setColor (num, cA: _ list) =
    if areColors then
      let rgb = if cA.Length = 0 then [255uy, 255uy, 255uy, 255uy] else cA
      //printfn "oldColors = %d\tcolors = %d\tappends = %d" (colors.GetNumberOfTuples()) num (append.GetOutput().GetNumberOfPoints())
      for i in 0 .. num - 1 do
        let r,g,b,a =
          let id = float i / float num * float rgb.Length |> int |> max 0 |> min (rgb.Length - 1)
          rgb.[id]
        colors.InsertNextValue r |> ignore
        colors.InsertNextValue g |> ignore
        colors.InsertNextValue b |> ignore
        colors.InsertNextValue a |> ignore

  let rotShift (cog, axisNormals) =
    let axisNormals = defaultFn axisNormals (fun () -> array2D [[1.; 0.; 0.]; [0.;1.;0.]; [0.;0.;1.]])
    let mat = vtkMatrix4x4.New ()
    axisNormals |> Array2D.iteri (fun m n v -> mat.SetElement (m,n,v) )
    mat.SetElement (3,3,1.)
    if mat.Determinant () < 0. then for i in 0 .. 2 do mat.SetElement (i, 2, - mat.GetElement(i,2))
    (orn + cog) |> Triple.iteri (fun i v -> mat.SetElement (i, 3, v))
    mat

  member o.Cylinders = cylinders
  member o.Spheres = spheres
  member o.Ellipsoids = ellipsoids
  member o.Lines = lines
  member o.AppendPolyData = append

  member o.colorsUpdate () =
    if areColors then append.GetOutput().GetPointData().SetScalars colors |> ignore
    else failwith "colorsUpdate: Set 'areColors' to true."

  member o.addCylinder (cog: float triple, radius, length, ?rotate: float [,], ?colors, ?postProcess) =
    let obj = new vtkCylinderSource ()
    obj.SetResolution 20
    obj.SetRadius radius
    obj.SetHeight length
    
    let tr = new vtkTransform ()
    use mat = 
      let rotate = rotate |> Option.map (fun rotate ->
        let newRot = rotate |> Array2D.copy
        for i = 0 to 2 do
          newRot.[i, 0] <- rotate.[i, 1]
          newRot.[i, 1] <- rotate.[i, 0]
        newRot )
      rotShift (cog, rotate)
    tr.SetMatrix mat
    let fil = new vtkTransformPolyDataFilter ()
    fil.SetTransform tr
    fil.SetInputConnection (obj.GetOutputPort())
    postProcess |> Option.iter (fun fn -> fn (obj, tr, fil))
    append.AddInputConnection (fil.GetOutputPort())
    append.Update()
    cylinders.Add (obj, tr, fil)
    setColor (obj.GetOutput().GetNumberOfPoints(), defaultArg colors [])

  member o.addSphere (cog: float triple, radius, ?rotate, ?colors, ?postProcess) =
    let obj, tr, fil = vtkSphereSource.New (), vtkTransform.New(), vtkTransformPolyDataFilter.New()
    obj.SetRadius radius
    use mat = rotShift (cog, rotate)
    tr.SetMatrix mat
    fil.SetTransform tr
    fil.SetInputConnection (obj.GetOutputPort())
    postProcess |> Option.iter (fun fn -> fn (obj, tr, fil))
    append.AddInputConnection (fil.GetOutputPort())
    append.Update()
    spheres.Add (obj, tr, fil)
    setColor (obj.GetOutput().GetNumberOfPoints(), defaultArg colors [])
  
  member o.addEllipsoid (cog: float triple, halfAxisLengths: float [], axisNormals: float [,], ?colors, ?postProcess) =
    let obj, tr, fil = vtkSphereSource.New (), vtkTransform.New(), vtkTransformPolyDataFilter.New()
    obj.SetPhiResolution 20; obj.SetThetaResolution 20
    use mat = rotShift (cog, Some axisNormals)
    tr.SetMatrix mat
    tr.Scale (halfAxisLengths.[0], halfAxisLengths.[1], halfAxisLengths.[2])
    fil.SetTransform tr
    postProcess |> Option.iter (fun fn -> fn (obj, tr, fil))
    append.AddInputConnection (fil.GetOutputPort())
    append.Update()
    ellipsoids.Add (obj, tr, fil)
    setColor (obj.GetOutput().GetNumberOfPoints(), defaultArg colors [])

//  member o.addOrientationEllipoid (cog: float triple, lines: float triple [], ?colors, ?postProcess) =
//    OrientationEllipse.pToEllipsoid lines |> Option.iter (fun (vals, vecs) ->
//      o.addEllipsoid (cog, vals, vecs, ?colors = colors, ?postProcess = postProcess) )

  member o.addPolygon (xyz: _ [], ?colors, ?postProcess) =
    let points = new vtkPoints ()
    for x,y,z in xyz do points.InsertNextPoint (orn.x + x, orn.y + y, orn.z + z) |> ignore
    let poly = new vtkPolyData ()
    poly.SetPoints points
    let del3D = new vtkDelaunay3D ()
    del3D.SetInput poly
    let surf = new vtkDataSetSurfaceFilter ()
    surf.SetInputConnection (del3D.GetOutputPort())
    postProcess |> Option.iter (fun fn -> fn (points, poly, del3D, surf))
    append.AddInputConnection (surf.GetOutputPort())
    append.Update()
    polygons.Add (points, poly, del3D, surf)
    setColor (surf.GetOutput().GetNumberOfPoints(), defaultArg colors [])
  
  member o.addPoints (xyz: _ [], ?colors, ?postProcess) =
    let pts = new vtkPoints ()
    let vtx = new vtkCellArray ()
    for x,y,z in xyz do 
      let id = pts.InsertNextPoint (orn.x + x, orn.y + y, orn.z + z)
      vtx.InsertNextCell 1 |> ignore
      vtx.InsertCellPoint id
    let poly = new vtkPolyData ()
    poly.SetPoints pts
    poly.SetVerts vtx
    postProcess |> Option.iter (fun fn -> fn (pts, vtx, poly))
    append.AddInput poly
    append.Update()
    points.Add (pts, vtx, poly)
    setColor (poly.GetNumberOfPoints(), defaultArg colors [])

  member o.addPlanarPolygon (xyz: _ [], ?colors, ?postProcess) =
    let points = new vtkPoints ()
    let polygon = new vtkPolygon ()
    polygon.GetPointIds().SetNumberOfIds xyz.Length
    xyz |> Array.iteri (fun i (x, y, z) ->
      points.InsertNextPoint (orn.x + x, orn.y + y, orn.z + z) |> ignore
      polygon.GetPointIds().SetId(i, i) |> ignore)
    let cells = new vtkCellArray()
    cells.InsertNextCell(polygon) |> ignore
    let poly = new vtkPolyData ()
    poly.SetPoints points
    poly.SetPolys cells
    postProcess |> Option.iter (fun fn -> fn (points, polygon, cells, poly))
    append.AddInput poly
    append.Update()
    planarPolygons.Add (points, polygon, cells, poly)
    setColor (poly.GetNumberOfPoints(), defaultArg colors [])

  member o.addPolygon (xyz: float triple [], ?colors, ?postProcess) = o.addPolygon (xyz |> Array.map Triple.toTuple, ?colors = colors, ?postProcess = postProcess)
  member o.addPlanarPolygon (xyz: float triple [], ?colors, ?postProcess) = o.addPlanarPolygon (xyz |> Array.map Triple.toTuple, ?colors = colors, ?postProcess = postProcess)
  member o.addPoints (xyz: float triple [], ?colors, ?postProcess) = o.addPoints (xyz |> Array.map Triple.toTuple, ?colors = colors, ?postProcess = postProcess)

  member o.addLines (num, iter) =
    let points = vtkPoints.New()
    let poly = vtkPolyData.New()
    poly.Allocate (num, 100)
    
    let la = List ()
    iter (fun (p1: float triple, p2: float triple) colors ->
      let p1, p2 = orn + p1, orn + p2
      let id1, id2 = points.InsertNextPoint (p1.x, p1.y, p1.z), points.InsertNextPoint (p2.x, p2.y, p2.z)
      let line = vtkIdList.New()
      line.InsertNextId id1 |> ignore
      line.InsertNextId id2 |> ignore
      poly.InsertNextCell (3, line) |> ignore
      setColor (2, colors)
      la.Add line )
    
    poly.SetPoints points
    append.AddInput poly
    append.Update()
    lines.Add (points, poly, la)

  member o.addLines (lines: (float triple * float triple) [], ?colors: _ []) =
    o.addLines (lines.Length, fun fn -> lines |> Array.iteri (fun i data ->
      let cA = match colors with Some cAA -> [cAA.[i]] | None -> []
      fn data cA))

  member o.saveToFile path =
    (System.IO.FileInfo path).Directory.Create ()
    use vtk = new vtkXMLPolyDataWriter()
    vtk.SetCompressorTypeToZLib()
    vtk.SetDataModeToAppended()
    vtk.AddInputConnection (o.AppendPolyData.GetOutputPort())
    vtk.SetFileName path
    vtk.Write() |> ignore

  interface System.IDisposable with
    member o.Dispose() =
      append.Dispose(); colors.Dispose()
      lines.ForEach (fun (a,b,c) -> a.Dispose(); b.Dispose(); c.ForEach (fun d -> d.Dispose()))
      cylinders.ForEach (fun (a,b,c) -> a.Dispose(); b.Dispose(); c.Dispose())
      spheres.ForEach (fun (a,b,c) -> a.Dispose(); b.Dispose(); c.Dispose())
      ellipsoids.ForEach (fun (a,b,c) -> a.Dispose(); b.Dispose(); c.Dispose())
      polygons.ForEach (fun (a,b,c,d) -> a.Dispose(); b.Dispose(); c.Dispose(); d.Dispose())
      planarPolygons.ForEach (fun (a,b,c,d) -> a.Dispose(); b.Dispose(); c.Dispose(); d.Dispose())
      points.ForEach (fun (a,b,c) -> a.Dispose(); b.Dispose(); c.Dispose())

type ImageData (dx, dy, dz, realOrigin: float triple) =

  let vti =
    let a = vtkImageData.New()
    a.SetSpacing (dx,dy,dz); a

  let addData1F32 (data: _ array3, label: string) =
    vti.SetExtent (0, data.lx, 0, data.ly 0, 0, data.lz(0,0))
    use arr = new vtkFloatArray ()
    arr.SetNumberOfComponents 1
    arr.SetNumberOfValues (data.lx * data.ly 0 * data.lz(0,0))
    arr.SetName label
    let mutable id = 0
    for z = 0 to data.lz(0,0) - 1 do
      for y = 0 to data.ly 0 - 1 do
        for x = 0 to data.lx - 1 do
          arr.SetValue(id, data.[x,y,z]); id <- id + 1
    vti.GetCellData().AddArray arr |> ignore

  let addData2F32 ((data: _ array3), label: string) =
    vti.SetExtent (0, data.lx, 0, data.ly 0, 0, data.lz(0,0))
    use arr = new vtkFloatArray()
    arr.SetNumberOfComponents 3
    arr.SetNumberOfValues (3 * data.lx * data.ly 0 * data.lz(0,0))
    arr.SetName label
    let mutable id = 0
    for z = 0 to data.lz(0,0) - 1 do
      for y = 0 to data.ly 0 - 1 do
        for x = 0 to data.lx - 1 do
          let gx, gy, gz = data.[x,y,z]
          arr.SetValue(id, gx); id <- id + 1
          arr.SetValue(id, gy); id <- id + 1
          arr.SetValue(id, gz); id <- id + 1
    vti.GetCellData().AddArray arr |> ignore

  let addData3F32 (gx: _ array3, gy: _ array3, gz: _ array3, label: string) =
    vti.SetExtent (0, gx.lx, 0, gx.ly 0, 0, gx.lz(0,0))
    use arr = new vtkFloatArray()
    arr.SetNumberOfComponents 3
    arr.SetNumberOfValues (3 * gx.lx * gx.ly 0 * gx.lz(0,0))
    arr.SetName label
    let mutable id = 0
    for z = 0 to gx.lz(0,0) - 1 do
      for y = 0 to gx.ly 0 - 1 do
        for x = 0 to gx.lx - 1 do
          arr.SetValue(id, gx.[x,y,z]); id <- id + 1
          arr.SetValue(id, gy.[x,y,z]); id <- id + 1
          arr.SetValue(id, gz.[x,y,z]); id <- id + 1
    vti.GetCellData().AddArray arr |> ignore

  let addData1B (data: _ array3, label: string) =
    vti.SetExtent (0, data.lx, 0, data.ly 0, 0, data.lz(0,0))
    use arr = new vtkUnsignedCharArray ()
    arr.SetNumberOfComponents 1
    arr.SetNumberOfValues (data.lx * data.ly 0 * data.lz(0,0))
    arr.SetName label
    let mutable id = 0
    for z = 0 to data.lz(0,0) - 1 do
      for y = 0 to data.ly 0 - 1 do
        for x = 0 to data.lx - 1 do
          arr.SetValue(id, data.[x,y,z]); id <- id + 1
    vti.GetCellData().AddArray arr |> ignore

  let addData2B ((data: _ array3), label: string) =
    vti.SetExtent (0, data.lx, 0, data.ly 0, 0, data.lz(0,0))
    use arr = new vtkUnsignedCharArray()
    arr.SetNumberOfComponents 3
    arr.SetNumberOfValues (3 * data.lx * data.ly 0 * data.lz(0,0))
    arr.SetName label
    let mutable id = 0
    for z = 0 to data.lz(0,0) - 1 do
      for y = 0 to data.ly 0 - 1 do
        for x = 0 to data.lx - 1 do
          let gx, gy, gz = data.[x,y,z]
          arr.SetValue(id, gx); id <- id + 1
          arr.SetValue(id, gy); id <- id + 1
          arr.SetValue(id, gz); id <- id + 1
    vti.GetCellData().AddArray arr |> ignore

  let addData3B (gx: _ array3, gy: _ array3, gz: _ array3, label: string) =
    use arr = new vtkUnsignedCharArray()
    arr.SetNumberOfComponents 3
    arr.SetNumberOfValues (3 * gx.lx * gx.ly 0 * gx.lz(0,0))
    arr.SetName label
    let mutable id = 0
    for z = 0 to gx.lz(0,0) - 1 do
      for y = 0 to gx.ly 0 - 1 do
        for x = 0 to gx.lx - 1 do
          arr.SetValue(id, gx.[x,y,z]); id <- id + 1
          arr.SetValue(id, gy.[x,y,z]); id <- id + 1
          arr.SetValue(id, gz.[x,y,z]); id <- id + 1
    vti.GetCellData().AddArray arr |> ignore

  member o.ImageData = vti

  member o.changeOrigin (x,y,z) = vti.SetOrigin(x,y,z)

  member o.addCellData (data, label) = addData1F32(data, label)
  member o.addCellData (data, label) = addData2F32(data, label)
  member o.addCellData (ux, uy, uz, label) = addData3F32(ux, uy, uz, label)

  member o.addCellData (data, label) = addData1B(data, label)
  member o.addCellData (data, label) = addData2B(data, label)
  member o.addCellData (ux, uy, uz, label) = addData3B(ux, uy, uz, label)

  member o.saveToFile path =
    (System.IO.FileInfo path).Directory.Create ()
    use vtx = new vtkXMLImageDataWriter()
    vtx.SetCompressorTypeToZLib()
    vtx.SetDataModeToAppended()
    vti.SetOrigin (realOrigin.x - 0.5 * dx, realOrigin.y - 0.5 * dy, realOrigin.z - 0.5 * dz)
    vtx.SetInput vti
    vtx.SetFileName path
    vtx.Write() |> ignore

  interface System.IDisposable with
    member o.Dispose() =
      vti.Dispose()


type View3D (control: System.Windows.Forms.Control, ?axisColor, ?rotationHorVert, ?backgroundColor) =

  let renderWindowControl = new Kitware.VTK.RenderWindowControl()
  let mapper = vtkPolyDataMapper.New()
  let actor = vtkActor.New()
  let renderWindowInteractor = vtkRenderWindowInteractor.New()
  let widget = vtkOrientationMarkerWidget.New()
  let axes = vtkAxesActor.New()

  let renderer = ref None
  let renderWindow = ref None

  let get (v: _ option ref) =
    let isNone () = Option.isNone v.Value
    let mutable i = 0
    while isNone () do
      i <- i + 1
      System.Threading.Thread.Sleep 50
      if i > 50 then failwith "Not succesfull to get either renderer or renderWindow..."
    v.Value.Value

  do
    control.Controls.Add renderWindowControl
    renderWindowControl.Load.Add (fun _ ->

      renderWindowControl.Dock <- System.Windows.Forms.DockStyle.Fill

      let rend = renderWindowControl.RenderWindow.GetRenderers().GetFirstRenderer()
      let renderWind = renderWindowControl.RenderWindow

      actor.SetMapper mapper
      actor.GetProperty().SetOpacity 0.9999
      rend.AddActor actor
      renderWind.AddRenderer rend
      renderWindowInteractor.SetRenderWindow renderWind

      renderWindowInteractor.SetInteractorStyle (vtkInteractorStyleTrackballCamera.New())

      rotationHorVert |> Option.iter (fun (hor, vert) ->
        rend.GetActiveCamera().Azimuth hor
        rend.GetActiveCamera().Elevation vert )
      
      backgroundColor |> Option.iter (fun (r,g,b) -> rend.SetBackground (r,g,b))
    
      axisColor |> Option.iter (fun (r,g,b) ->
        axes.GetXAxisCaptionActor2D().GetCaptionTextProperty().SetColor (r,g,b)
        axes.GetYAxisCaptionActor2D().GetCaptionTextProperty().SetColor (r,g,b)
        axes.GetZAxisCaptionActor2D().GetCaptionTextProperty().SetColor (r,g,b)
        widget.SetOrientationMarker axes
        widget.SetInteractor renderWindowInteractor 
        widget.SetViewport ( 0.0, 0.0, 0.4, 0.4 )
        widget.SetEnabled 1
        widget.InteractiveOff() )

      renderWindowInteractor.Initialize ()

      let flipX, flipY, flipZ = ref 0, ref 0, ref 0

      let resetView vs =
        let fn flip = flip := if !flip = 0 then 1 else 0
        let v, n =
          match vs with
            | "x" when !flipX = 0 ->  fn flipX; triple (1,0,0), triple (0,0,1)
            | "x" ->                  fn flipX; triple (-1,0,0), triple (0,0,1)
            | "y" when !flipY = 0 ->  fn flipY; triple (0,1,0), triple (0,0,1)
            | "y" ->                  fn flipY; triple (0,-1,0), triple (0,0,1)
            | "z" when !flipZ = 0 ->  fn flipZ; triple (0,0,-1), triple (0,1,0)
            | "z" ->                  fn flipZ; triple (0,0,1), triple (0,1,0)
            | _ -> failwithf "resetView: not supported = %s" vs
        let fp = Triple.ofArray (rend.GetActiveCamera().GetFocalPoint())
        let pos = fp - fp .* Triple.toFloat v
        rend.GetActiveCamera().SetPosition (pos.x, pos.y, pos.z)
        rend.GetActiveCamera().SetViewUp (float n.x, float n.y, float n.z)
        rend.ResetCamera ()
        renderWind.Render()
      
      rend.add_ModifiedEvt (vtkObject.vtkObjectEventHandler (fun a b -> flipX := 0; flipY := 0; flipZ := 0 ))

      renderWindowInteractor.add_KeyReleaseEvt (vtkObject.vtkObjectEventHandler (fun a b ->
        let key = renderWindowInteractor.GetKeySym()
        //printfn "add_KeyReleaseEvt = %s" key
      
        match key with
  //        | "space" ->
  //          if !isPlaying then isPlaying := false; renderWindowInteractor.DestroyTimer () |> ignore
  //          else isPlaying := true; renderWindowInteractor.CreateRepeatingTimer (uint32 10) |> ignore
          | "r" -> rend.ResetCamera()
          | "x" | "y" | "z" -> resetView key
//          | "t" ->
//            dimId := (!dimId + 1) % (ellsVTK.GetLength toggleDim + 1)
//            update !index
          | "Escape" -> System.Environment.Exit 0
          | _ -> () ))

      renderer := Some rend
      renderWindow := Some renderWind )

  member o.Renderer = get renderer
  member o.RenderWindow = get renderWindow
  member o.RenderWindowControl = renderWindowControl
  member o.Mapper = mapper
  member o.Actor = actor
  member o.RenderWindowInteractor = renderWindowInteractor

  member o.saveImage fileName =
    use image = vtkRenderLargeImage.New()
    use jpg = vtkPNGWriter.New()
    image.SetInput o.Renderer
    image.SetMagnification 5
    jpg.SetInput (image.GetOutput())
    jpg.SetFileName fileName
    jpg.Write ()

  member o.display () = ()

  interface System.IDisposable with
    member o.Dispose () =
      axes.Dispose ()
      widget.Dispose ()
      renderer.Value |> Option.iter (fun v -> v.Dispose ())
      renderWindow.Value |> Option.iter (fun v -> v.Dispose ())
      renderWindowInteractor.Dispose ()
