﻿namespace Kit.Tests.Collections

open NUnit.Framework
open Kit.Collections
open Kit

[<TestFixture>]
type Array3D_Tests() =

  let indexChecker() =
    let m, n = ref 0, ref 0
    let runner (t: _ triple) value =
      incr n
      m := !m + !n * (1000000 * t.x + 10000 * t.y + 100 * t.z + value)
    let result() = !m
    runner, result

  let pathChecker() =
    let m, n = ref 0, ref 0
    let runner value =
      incr n
      m := !m + !n * value
    runner, (fun () -> !m)

  [<Test>]
  member o.sumBy() =
    let runner, ended = pathChecker()
    let arrayIn = Array3D.init 2 2 2 (fun i j k -> 100 * i + 10 * j + k)
    let output = arrayIn |> Array3D.sumBy (fun value ->
      runner value
      value )
    Assert.That(output, Is.EqualTo 444)
    Assert.That(ended(), Is.EqualTo 2840)

  [<Test>]
  member o.sumByi() =
    let runner, ended = indexChecker()
    let arrayIn = Array3D.init 2 2 2 (fun i j k -> 100 * i + 10 * j + k)
    let output = arrayIn |> Array3D.sumByi (fun xyz value ->
      runner xyz value
      value )
    Assert.That(output, Is.EqualTo 444)
    Assert.That(ended(), Is.EqualTo 26224840)

  [<Test>]
  member o.zip() =
    let arrayIn1 = Array3D.init 2 2 2 (fun i j k -> 100 * i + 10 * j + k)
    let arrayIn2 = Array3D.init 2 2 2 (fun i j k -> i + 10 * j + 100 * k)
    let arrayIn = Array3D.zip(arrayIn1, arrayIn2)
    let arrayOut = Array3D.init 2 2 2 (fun i j k -> 100 * i + 10 * j + k, i + 10 * j + 100 * k)
    Assert.That(arrayIn, Is.EqualTo arrayOut)