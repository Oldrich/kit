﻿namespace Kit.Tests.Collections

open NUnit.Framework
open Kit.Collections

[<TestFixture>]
type Array_Tests() =

  [<Test>]
  member o.averageByi() =
    let arrayIn = [| 0; 1; 2; 3 |]
    let average = arrayIn |> Array.averageByi (fun i value -> float (i * value))
    Assert.That(average, Is.InRange(3.49, 3.51))

  [<Test>]
  member o.choosei() =
    let arrayIn = [| 1; 2; 3 |]
    let result = arrayIn |> Array.choosei (fun i value ->
      if i > 1 then Some (i + value) else None
    )
    Assert.That(result, Is.EquivalentTo [| 5 |])

  [<Test>]
  member o.collecti() =
    let arrayIn = [| 1; 2; 3 |]
    let result =
      arrayIn |> Array.collecti (fun i value ->
        [| i; value |]
      )
    Assert.That(result, Is.EquivalentTo [| 0; 1; 1; 2; 2; 3 |])

  [<Test>]
  member o.distinct() =
    let arrayIn = [| 3; 1; 2; 3; 2; 3 |]
    let result = arrayIn |> Array.distinct
    Assert.That(result, Is.EquivalentTo [| 3; 1; 2 |])

  [<Test>]
  member o.distinctBy() =
    let arrayIn = [| "3"; "1"; "2"; "3"; "2"; "3" |]
    let result = arrayIn |> Array.distinctBy int
    let equal = result = [| "3"; "1"; "2" |]
    Assert.That(result, Is.EquivalentTo [| "3"; "1"; "2" |])
    Assert.That(equal)

  [<Test>]
  member o.existsi() =
    let arrayIn = [| 1; 2; 3 |]
    let result = arrayIn |> Array.existsi (fun i value ->
      i + value = 3
    )
    Assert.That(result)

  [<Test>]
  member o.filteri() =
    let arrayIn = [| 1; 2; 3 |]
    let filtered = arrayIn |> Array.filteri (fun i text -> i > 1)
    Assert.That(filtered.Length, Is.EqualTo 1)
    Assert.That(filtered.[0], Is.EqualTo 3)

  [<Test>]
  member o.foldi() =
    let arrayIn = [| 1; 2; 3 |]
    let result =
      arrayIn |> Array.foldi (fun i folded value ->
        i * value + folded ) 5
    Assert.That(result, Is.EqualTo 13)

  [<Test>]
  member o.linspace() =
    let result = Array.linspace(5.123, 8.543, 3)
    Assert.That(result, Is.EquivalentTo [| 5.123; 6.833; 8.543 |])

  [<Test>]
  member o.maxByi() =
    let arrayIn = [| "3"; "1"; "1" |]
    let result = arrayIn |> Array.maxByi (fun i value ->
      int value + i
    )
    Assert.That(result, Is.EqualTo "3")

  [<Test>]
  member o.minByi() =
    let arrayIn = [| "3"; "1"; "0" |]
    let result = arrayIn |> Array.minByi (fun i value ->
      int value + i
    )
    Assert.That(result, Is.EqualTo "1")

  [<Test>]
  member o.partitioni() =
    let arrayIn = [| "3"; "1"; "0"; "2" |]
    let result1, result2 = arrayIn |> Array.partitioni (fun i value ->
      int value + i > 3
    )
    Assert.That(result1, Is.EquivalentTo [| "2" |])
    Assert.That(result2, Is.EquivalentTo [| "3"; "1"; "0" |])

  [<Test>]
  member o.removeOption() =
    let arrayIn = [| Some "3"; None; Some "1"; Some "0" |]
    let result = arrayIn |> Array.removeOption "0"
    Assert.That(result, Is.EquivalentTo [| "3"; "0"; "1"; "0" |])

  [<Test>]
  member o.sum() =
    let result = Array.sum [| 3; 1; 6 |]
    Assert.That(result, Is.EqualTo 10)

  [<Test>]
  member o.sumBy() =
    let arrayIn = [| "3"; "1"; "6" |]
    let result = arrayIn |> Array.sumBy (fun value ->
      int value )
    Assert.That(result, Is.EqualTo 10)

  [<Test>]
  member o.sumByi() =
    let arrayIn = [| "3"; "1"; "0" |]
    let result = arrayIn |> Array.sumByi (fun i value ->
      int value + i
    )
    Assert.That(result, Is.EqualTo 7)

  [<Test>]
  member o.tail() =
    let arrayIn = [| "3"; "1"; "0" |]
    let result = arrayIn |> Array.tail
    Assert.That(result, Is.EquivalentTo [| "1"; "0" |])