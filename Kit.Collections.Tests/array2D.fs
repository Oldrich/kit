﻿namespace Kit.Tests.Collections

open NUnit.Framework
open Kit.Collections

[<TestFixture>]
type Array2D_Tests() =

  let indexChecker() =
    let m, n = ref 0, ref 0
    let runner (x, y) value =
      incr n
      m := !m + !n * (10000 * x + 100 * y + value)
    let result() = !m
    runner, result

  let pathChecker() =
    let m, n = ref 0, ref 0
    let runner value =
      incr n
      m := !m + !n * value
    runner, (fun () -> !m)

  [<Test>]
  member o.invert3x3() =
    let arrayIn = array2D [ [1.; 8.; 2.]; [4.; 3.; 5.]; [6.; 2.; 10.] ]
    let result = arrayIn |> Array2D.invert3x3
    let shouldBe = array2D [ [-0.25; 0.95; -0.425]; [0.125; 0.025; -0.0375]; [0.125; -0.575; 0.3625] ]
    Assert.That(result, Is.EqualTo(shouldBe).Within(1e-10))

  [<Test>]
  member o.max() =
    let arrayIn = array2D [ [1; 8; 2]; [4; 3; 5] ]
    let result = arrayIn |> Array2D.max
    Assert.That(result, Is.EqualTo 8)

  [<Test>]
  member o.maxBy() =
    let input = array2D [ [1; 8; 2]; [4; 12; 5]; [6; 2; 10] ]
    let runner, result = pathChecker()
    let output = input |> Array2D.maxBy (fun value ->
      runner value
      float value )
    Assert.That(output, Is.EqualTo 12)
    Assert.That(result(), Is.EqualTo 277)

  [<Test>]
  member o.maxByi() =
    let input = array2D [ [1; 8; 2]; [4; 12; 5]; [6; 2; 10] ]
    let runner, result = indexChecker()
    let output = input |> Array2D.maxByi (fun ij value ->
      runner ij value
      float value
    )
    Assert.That(output, Is.EqualTo 12)
    Assert.That(result(), Is.EqualTo 635377)

  [<Test>]
  member o.min() =
    let arrayIn = array2D [ [1; 8; 2]; [4; -5; 5] ]
    let result = arrayIn |> Array2D.min
    Assert.That(result, Is.EqualTo -5)

  [<Test>]
  member o.minBy() =
    let input = array2D [ [1; 8; 2]; [4; 12; 5]; [6; 2; 10] ]
    let runner, result = pathChecker()
    let output = input |> Array2D.minBy (fun value ->
      runner value
      float value )
    Assert.That(output, Is.EqualTo 1)
    Assert.That(result(), Is.EqualTo 277)

  [<Test>]
  member o.minByi() =
    let input = array2D [ [1; 8; 2]; [4; 12; 5]; [6; 2; 10] ]
    let runner, result = indexChecker()
    let output = input |> Array2D.minByi (fun ij value ->
      runner ij value
      float value
    )
    Assert.That(output, Is.EqualTo 1)
    Assert.That(result(), Is.EqualTo 635377)

  [<Test>]
  member o.mult() =
    let arrayIn1 = array2D [ [1; 8; 2]; [4; 3; 5] ]
    let arrayIn2 = array2D [ [1; 2]; [4; 3]; [10; 2] ]
    let result = Array2D.mult(arrayIn1, arrayIn2)
    let shouldBe = array2D [ [53; 30]; [66; 27] ]
    Assert.That(result, Is.EqualTo shouldBe)

  [<Test>]
  member o.removeOption() =
    let input = array2D [ [Some 1; Some 8; Some 2]; [Some 4; Some 3; None] ]
    let output = input |> Array2D.removeOption -1
    let shouldBe = array2D [ [1; 8; 2]; [4; 3; -1] ]
    Assert.That(output, Is.EqualTo shouldBe)

  [<Test>]
  member o.sum() =
    let arrayIn = array2D [ [1; 8; 2]; [4; 3; 5] ]
    let result = arrayIn |> Array2D.sum
    Assert.That(result, Is.EqualTo 23)

  [<Test>]
  member o.sumBy() =
    let runner, ended = pathChecker()
    let arrayIn = array2D [ [1; 8; 2]; [4; 3; 5] ]
    let result =
      arrayIn |> Array2D.sumBy (fun value ->
        runner value
        float value )
    Assert.That(result, Is.EqualTo 23.0)
    Assert.That(ended(), Is.EqualTo 84)

  [<Test>]
  member o.sumByi() =
    let runner, ended = indexChecker()
    let arrayIn = array2D [ [1; 8; 2]; [4; 3; 5] ]
    let result =
      arrayIn |> Array2D.sumByi (fun (i,j) value ->
        runner (i,j) value
        float value )
    Assert.That(result, Is.EqualTo 23.0)
    Assert.That(ended(), Is.EqualTo 152584)

  [<Test>]
  member o.transpose() =
    let arrayIn = array2D [ [1; 8; 2]; [4; 3; 5] ]
    let result = arrayIn |> Array2D.transpose
    let shouldBe = array2D [ [1; 4]; [8; 3]; [2; 5] ]
    Assert.That(result, Is.EqualTo shouldBe)